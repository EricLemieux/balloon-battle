.PHONY: build test lint

build:
	make -C game-server build

test:
	make -C game-server test

lint:
	make -C game-server lint
